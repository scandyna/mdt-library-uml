UML for [multidiagtools](https://github.com/scandyna/multidiagtools) library.


Because Bouml seems not to be active anymore,
I have to move to something else.

I decided to use PlantUML, because it seems active and widely used.
Also, UML as code seems better.

A overview is available here:
[Doxygen view](https://scandyna.gitlab.io/mdt-library-uml/)
